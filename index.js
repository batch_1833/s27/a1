let http = require("http");

const port = 4000;

http.createServer(function (request, response) {
	if(request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'});

		response.write(`Welcome to booking system`);
		response.end();
	}

	if(request.url == "/profile" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'});

		response.write(`Welcome to your profile`);
		response.end();
	}

	if(request.url == "/courses" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'});

		response.write(`Here's our courses available`);
		response.end();
	}

	if(request.url = "/addCourse" && request.method == "POST"){

		response.writeHead(200, {'Content-Type': 'text/plain'});

		response.write(`Add course to our resources`);
		response.end();
	}

	if(request.url = "/updateCourse" && request.method == "PUT"){

		response.writeHead(200, {'Content-Type': 'text/plain'});

		response.write(`Update a course to our resources`);
		response.end();
	}

	if(request.url = "/archiveCourses" && request.method == "DELETE"){

		response.writeHead(200, {'Content-Type': 'text/plain'});

		response.write(`Archive courses to our resources`);
		response.end();
	}

}).listen(port);
console.log(`Server running at localhost: ${port}`);
